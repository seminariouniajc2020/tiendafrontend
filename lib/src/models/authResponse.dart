class AuthResponse {
  String accessToken;
  String prefix;
  String message;

  AuthResponse({this.accessToken, this.prefix});
  AuthResponse.error({this.message});

  factory AuthResponse.fromJson(Map<String, dynamic> json) {
    return AuthResponse(
        accessToken: json['accessToken'], prefix: json['prefix']);
  }

  factory AuthResponse.fromJsonError(Map<String, dynamic> json) {
    return AuthResponse.error(message: json['message']);
  }

  Map<String, dynamic> toJson() =>
      {"accessToken": accessToken, "prefix": prefix};
}
