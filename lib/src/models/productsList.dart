import 'package:tiendafrontend/src/models/products.dart';

class ProductsList {
  final List<Products> lstProductos;

  ProductsList({this.lstProductos});

  factory ProductsList.fromJson(List<dynamic> productos) {
    List<Products> lstProductos = new List<Products>();
    lstProductos = productos.map((i) => Products.fromJson(i)).toList();

    return new ProductsList(
      lstProductos: lstProductos,
    );
  }
}
