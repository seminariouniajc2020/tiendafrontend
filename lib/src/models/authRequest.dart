class AuthRequest {
  String email;
  String password;

  AuthRequest({this.email, this.password});

  factory AuthRequest.fromJson(Map<String, dynamic> json) {
    return AuthRequest(email: json['email'], password: json['password']);
  }

  Map<String, dynamic> toJson() => {"email": email, "password": password};
}
