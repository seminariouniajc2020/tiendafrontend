class Products {
  String calidad;
  int cantidad;
  String caracteristica;
  String color;
  int id;
  int idEmpresa;
  String imagen;
  String nombre;
  int precio;
  String talla;
  String tipoProducto;
  String message;

  Products(
      {this.calidad,
      this.cantidad,
      this.caracteristica,
      this.color,
      this.id,
      this.idEmpresa,
      this.imagen,
      this.nombre,
      this.precio,
      this.talla,
      this.tipoProducto});

  Products.error({this.message});

  factory Products.fromJson(Map<String, dynamic> json) {
    return Products(
        calidad: json['calidad'],
        cantidad: json['cantidad'],
        caracteristica: json['caracteristica_persona'],
        color: json['color'],
        id: json['id'],
        idEmpresa: json['idEmpresa'],
        imagen: json['imagen'],
        nombre: json['nombre'],
        precio: json['precio'],
        talla: json['talla'],
        tipoProducto: json['tipo_de_producto']);
  }

  factory Products.fromJsonError(Map<String, dynamic> json) {
    return Products.error(message: json['message']);
  }

  Map<String, dynamic> toJson() => {
        "calidad": calidad,
        "cantidad": cantidad,
        "caracteristica": caracteristica,
        "color": color,
        "id": id,
        "idEmpresa": idEmpresa,
        "imagen": imagen,
        "nombre": nombre,
        "precio": precio,
        "talla": talla,
        "tipoProducto": tipoProducto
      };
}
