class BuyRequest {
  int idProducto;
  int idUser;
  int cantidad;

  BuyRequest({this.idProducto, this.idUser, this.cantidad});

  factory BuyRequest.fromJson(Map<String, dynamic> json) {
    return BuyRequest(
        idProducto: json['idProducto'],
        idUser: json['idUser'],
        cantidad: json['cantidad']);
  }

  Map<String, dynamic> toJson() =>
      {"idProducto": idUser, "idUser": idUser, "cantidad": cantidad};
}
