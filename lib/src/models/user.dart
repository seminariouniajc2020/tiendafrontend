class User {
  int id;
  String identification;
  String name;
  String lastName;
  String phone;
  String picture;
  int credits;
  String email;
  bool enable;
  String message;

  User(
      {this.id,
      this.identification,
      this.name,
      this.lastName,
      this.phone,
      this.picture,
      this.credits,
      this.email,
      this.enable});
  User.error({this.message});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'],
        identification: json['identification'],
        name: json['name'],
        lastName: json['lastName'],
        phone: json['phone'],
        picture: json['picture'],
        credits: json['credits'],
        email: json['email'],
        enable: json['enable']);
  }

  factory User.fromJsonError(Map<String, dynamic> json) {
    return User.error(message: json['message']);
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "identification": identification,
        "name": name,
        "lastName": lastName,
        "phone": phone,
        "picture": picture,
        "credits": credits,
        "email": email,
        "enable": enable
      };
}
