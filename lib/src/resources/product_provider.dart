import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:tiendafrontend/src/Constants.dart';
import 'package:tiendafrontend/src/models/productsList.dart';

class ProductProvider {
  Client client = Client();
  ProductsList _products;

  Future<ProductsList> infoProducts(String accesToken) async {
    Uri uri = Uri.http(Constants.urlAuthority, Constants.consultarProductos);
   
    var res = await client.get(uri,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accesToken});

    if (res.statusCode == 200) {
      // If the call to the server was successful, parse the JSON

      _products = ProductsList.fromJson(jsonDecode(res.body));
    } else {
      print(res.body);
    }
    return _products;
  }
}
