import 'package:tiendafrontend/src/models/apiResponse.dart';
import 'package:tiendafrontend/src/models/authRequest.dart';
import 'package:tiendafrontend/src/models/buyRequest.dart';
import 'package:tiendafrontend/src/models/productsList.dart';
import 'package:tiendafrontend/src/models/user.dart';
import 'package:tiendafrontend/src/resources/product_provider.dart';

import 'auth_provider.dart';

class Repository {
  final AuthProvider authProvider = AuthProvider();
  final ProductProvider productProvider = ProductProvider();

  Future<ApiResponse> login(AuthRequest authRequest) =>
      authProvider.login(authRequest);

  Future<User> infoUser(String accesToken) => authProvider.infoUser(accesToken);

  Future<ProductsList> fetchAllProducts(String accesToken) =>
      productProvider.infoProducts(accesToken);

  Future<ApiResponse> updateInfo(User user, String accesToken) =>
      authProvider.updateInfo(user, accesToken);

  Future<ApiResponse> comprar(BuyRequest compra, String accesToken) =>
      authProvider.comprar(compra, accesToken);
}
