import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:tiendafrontend/src/Constants.dart';
import 'package:tiendafrontend/src/models/apiResponse.dart';
import 'package:tiendafrontend/src/models/authRequest.dart';
import 'package:tiendafrontend/src/models/buyRequest.dart';
import 'package:tiendafrontend/src/models/user.dart';

class AuthProvider {
  Client client = Client();
  ApiResponse _apiResponse;
  User _user;

  Future<ApiResponse> login(AuthRequest authRequest) async {
    var body = jsonEncode(authRequest.toJson());
    Uri uri = Uri.http(Constants.urlAuthority, Constants.urlAuth);

    var res = await client.post(uri,
        headers: {HttpHeaders.contentTypeHeader: Constants.contentTypeJson},
        body: body);

    _apiResponse = ApiResponse.fromJson({'data': res.body});
    return _apiResponse;
  }

  Future<User> infoUser(String accesToken) async {
    Uri uri = Uri.http(Constants.urlAuthority, Constants.urlUserInfo);
    var res = await client.get(uri,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accesToken});

    if (res.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      _user =
          User.fromJson(jsonDecode(utf8.decode(res.body.toString().codeUnits)));
    } else {
      _user = User.fromJsonError(jsonDecode(res.body));
    }
    return _user;
  }

  Future<ApiResponse> updateInfo(User user, String accesToken) async {
    var body = jsonEncode(user.toJson());
    Uri uri = Uri.http(Constants.urlAuthority, Constants.urlUserInfo);

    var res = await client.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeJson,
          HttpHeaders.authorizationHeader: "Bearer " + accesToken
        },
        body: body);

    _apiResponse =
        ApiResponse.fromJson({'data': res.body, 'statusCode': res.statusCode});
    return _apiResponse;
  }

  Future<ApiResponse> comprar(BuyRequest compra, String accesToken) async {
    var body = jsonEncode(compra.toJson());
    Uri uri = Uri.http(Constants.urlAuthority, Constants.compras);

    var res = await client.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeJson,
          HttpHeaders.authorizationHeader: "Bearer " + accesToken
        },
        body: body);

    print(res.statusCode);
    print(res.body);
    _apiResponse =
        ApiResponse.fromJson({'data': res.body, 'statusCode': res.statusCode});
    return _apiResponse;
  }
}
