import 'package:rxdart/rxdart.dart';
import 'package:tiendafrontend/src/blocs/authorization_bloc.dart';
import 'package:tiendafrontend/src/models/user.dart';
import 'package:tiendafrontend/src/resources/repository.dart';

class HomeBloc {
  final _repository = Repository();
  final _user = PublishSubject<User>();

  Stream<User> get infoUser => _user.stream;

  getInfoUser() async {
    String accesToken = await authBloc.getToken();
    User user = await _repository.infoUser(accesToken);
    await authBloc.setIdUser(user.id);
    _user.sink.add(user);
    return user;
  }

  logoutUser() {
    authBloc.closeSession();
  }
}

final homeBloc = HomeBloc();
