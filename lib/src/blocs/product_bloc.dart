import 'package:rxdart/rxdart.dart';
import 'package:tiendafrontend/src/blocs/authorization_bloc.dart';
import 'package:tiendafrontend/src/models/productsList.dart';
import 'package:tiendafrontend/src/resources/repository.dart';

class ProductsBloc {
  final _repository = Repository();
  final _producto = PublishSubject<ProductsList>();

  Stream<ProductsList> get infoProducts => _producto.stream;

  getInfoProducts() async {
    String accesToken = await authBloc.getToken();
    ProductsList producto = await _repository.fetchAllProducts(accesToken);
    _producto.sink.add(producto);
    return producto;
  }

  dispose() {
    _producto.close();
  }
}

final productsBloc = ProductsBloc();
