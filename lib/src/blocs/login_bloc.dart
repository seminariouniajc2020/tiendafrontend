import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tiendafrontend/src/Dialogs.dart';
import 'package:tiendafrontend/src/blocs/authorization_bloc.dart';
import 'package:tiendafrontend/src/blocs/validator.dart';
import 'package:tiendafrontend/src/models/apiResponse.dart';
import 'package:tiendafrontend/src/models/authRequest.dart';
import 'package:tiendafrontend/src/models/authResponse.dart';
import 'package:tiendafrontend/src/resources/repository.dart';

class LoginBloc extends Validators {
  Repository repository = Repository();

  final BehaviorSubject _emailController = BehaviorSubject<String>();
  final BehaviorSubject _passwordController = BehaviorSubject<String>();
  final PublishSubject _loadingData = PublishSubject<bool>();

  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  Stream<String> get email => _emailController.stream.transform(validateEmail);
  Stream<String> get password =>
      _passwordController.stream.transform(validatePassword);

  Stream<bool> get submitValid =>
      Rx.combineLatest2(email, password, (email, password) => true);
  Stream<bool> get loading => _loadingData.stream;

  void submit(BuildContext ctx) {
    final validEmail = _emailController.value;
    final validPassword = _passwordController.value;
    if (validEmail == null || validEmail == '') {
      _emailController.addError('Por favor ingrese un email');
    } else if (validPassword == null || validPassword == '') {
      _passwordController.addError('Por favor ingrese una contraseña');
    } else {
      AuthRequest user =
          new AuthRequest(email: validEmail, password: validPassword);
      _loadingData.sink.add(true);
      login(user, ctx);
    }
  }

  login(AuthRequest authRequest, BuildContext context) async {
    ApiResponse apiresponse = await repository.login(authRequest);
    _loadingData.sink.add(false);
    if (apiresponse.data != null) {
      var data = jsonDecode(apiresponse.data);
      if (data['accessToken'] != null) {
        AuthResponse authResponse = AuthResponse.fromJson(data);
        authBloc.openSession(authResponse);
      } else {
        Dialogs.showAlertDialog(context, "Error", data['message']);
        //_mostrarAlerta(context, data['message']);
      }
    }
  }

  void dispose() {
    _emailController.close();
    _passwordController.close();
    _loadingData.close();
  }
}
