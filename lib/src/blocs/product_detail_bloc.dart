import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tiendafrontend/src/Dialogs.dart';
import 'package:tiendafrontend/src/blocs/authorization_bloc.dart';
import 'package:tiendafrontend/src/blocs/validator.dart';
import 'package:tiendafrontend/src/models/apiResponse.dart';
import 'package:tiendafrontend/src/models/buyRequest.dart';
import 'package:tiendafrontend/src/resources/repository.dart';

class ProductDetailBloc extends Validators {
  final _repository = Repository();
  final BehaviorSubject _productController = BehaviorSubject<String>();
  final BehaviorSubject _cantidadController = BehaviorSubject<String>();

  Function(String) get changeCantidad => _cantidadController.sink.add;
  Function(String) get changeProduct => _productController.sink.add;

  Stream<String> get cantidad =>
      _cantidadController.stream.transform(validateFieldEmpty);
  Stream<String> get product =>
      _productController.stream.transform(validateFieldEmpty);
  Stream<bool> get submitValid =>
      Rx.combineLatest2(cantidad, product, (identification, product) => true);

  void submit(BuildContext context) async {
    final validProducto = _cantidadController.value;
    final validCantidad = _cantidadController.value;

    if (validProducto == null || validProducto == '') {
      Dialogs.showAlertDialog(context, "Error",
          "Ha ocurrido un error no hay producto seleccionado");
    }
    if (validCantidad == null || validCantidad == '') {
      _cantidadController.addError('Por favor ingrese una cantidad');
    } else {
      String idUser = await authBloc.getIdUser();
      BuyRequest compra = new BuyRequest(
          idProducto: int.parse(validProducto),
          idUser: int.parse(idUser),
          cantidad: int.parse(validCantidad));

      comprar(compra, context);
    }
  }

  comprar(BuyRequest buyRequest, BuildContext context) async {
    String accesToken = await authBloc.getToken();
    ApiResponse apiresponse = await _repository.comprar(buyRequest, accesToken);

    if (apiresponse.status == 200 || apiresponse.status == 201) {
      Dialogs.showAlertDialog(context, "Exito", "Compra Realizada");
    } else {
      var data = jsonDecode(apiresponse.data);
      Dialogs.showAlertDialog(
          context, "Error", utf8.decode(data['message'].toString().codeUnits));
    }
  }
}
