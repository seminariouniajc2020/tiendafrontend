import 'dart:async';

class Validators {
  final validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, skin) {
    if (email.contains("@")) {
      skin.add(email);
    } else {
      skin.addError("Email - error");
    }
  });

  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, skin) {
    if (password.length < 10) {
      skin.add(password);
    } else {
      skin.addError("Password - error");
    }
  });

  final validateFieldEmpty =
      StreamTransformer<String, String>.fromHandlers(handleData: (field, skin) {
    if (field.length > 0 && field.length < 20) {
      skin.add(field);
    } else {
      skin.addError("Por favor complete el campo");
    }
  });
}
