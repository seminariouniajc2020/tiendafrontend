import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tiendafrontend/src/Dialogs.dart';
import 'package:tiendafrontend/src/blocs/authorization_bloc.dart';
import 'package:tiendafrontend/src/models/apiResponse.dart';
import 'package:tiendafrontend/src/models/user.dart';
import 'package:tiendafrontend/src/resources/repository.dart';
import 'package:tiendafrontend/src/blocs/validator.dart';

class ProfileBloc extends Validators {
  final _repository = Repository();

  final _userFetcher = BehaviorSubject<User>();
  final BehaviorSubject _identificationController = BehaviorSubject<String>();
  final BehaviorSubject _nameController = BehaviorSubject<String>();
  final BehaviorSubject _lastNameController = BehaviorSubject<String>();
  final BehaviorSubject _phoneController = BehaviorSubject<String>();
  final BehaviorSubject _pictureController = BehaviorSubject<String>();
  final BehaviorSubject _creditsController = BehaviorSubject<String>();
  final PublishSubject _loadingData = PublishSubject<bool>();

  Function(String) get changeIdentification =>
      _identificationController.sink.add;
  Function(String) get changeName => _nameController.sink.add;
  Function(String) get changeLastName => _lastNameController.sink.add;
  Function(String) get changePhone => _phoneController.sink.add;
  Function(String) get changePicture => _pictureController.sink.add;
  Function(String) get changeCredits => _creditsController.sink.add;

  Stream<User> get userStream => _userFetcher.stream;

  Stream<String> get identification =>
      _identificationController.stream.transform(validateFieldEmpty);
  Stream<String> get name =>
      _nameController.stream.transform(validateFieldEmpty);
  Stream<String> get lastName =>
      _lastNameController.stream.transform(validateFieldEmpty);
  Stream<String> get phone =>
      _phoneController.stream.transform(validateFieldEmpty);
  Stream<String> get credits =>
      _creditsController.stream.transform(validateFieldEmpty);
  Stream<String> get picture =>
      _pictureController.stream.transform(validateFieldEmpty);
  Stream<bool> get loading => _loadingData.stream;

  Stream<bool> get submitValid =>
      Rx.combineLatest2(identification, name, (identification, name) => true);

  getInfoUser() async {
    String accesToken = await authBloc.getToken();
    User user = await _repository.infoUser(accesToken);
    if (user != null) {
      _userFetcher.sink.add(user);
      _identificationController.sink.add(user.identification);
      _nameController.sink.add(user.name);
      _lastNameController.sink.add(user.lastName);
      _phoneController.sink.add(user.phone);
      _creditsController.sink.add(user.credits.toString());
      _pictureController.sink.add(user.picture);
    }
  }

  void submit(BuildContext ctx) {
    final validIdentification = _identificationController.value;
    final validName = _nameController.value;
    final validLastName = _lastNameController.value;
    final validPhone = _phoneController.value;

    if (validIdentification == null || validIdentification == '') {
      _identificationController.addError('Por favor ingrese un documento');
    } else if (validName == null || validName == '') {
      _nameController.addError('Por favor ingrese su nombre');
    } else if (validLastName == null || validLastName == '') {
      _lastNameController.addError('Por favor ingrese su apelido');
    } else if (validPhone == null || validPhone == '') {
      _phoneController.addError('Por favor ingrese su número de telefono');
    } else {
      User user = _userFetcher.value;
      user.name = validName;
      user.lastName = validLastName;
      user.phone = validPhone;
      user.identification = validIdentification;

      print(user.toJson());
      _loadingData.sink.add(true);
      updateInfo(user, ctx);
    }
  }

  updateInfo(User user, BuildContext context) async {
    String accesToken = await authBloc.getToken();
    ApiResponse apiresponse = await _repository.updateInfo(user, accesToken);
    _loadingData.sink.add(false);
    if (apiresponse.status == 200) {
      Dialogs.showAlertDialog(context, "Exito", "Información Actualizada");
    } else {
      var data = jsonDecode(apiresponse.data);
      Dialogs.showAlertDialog(context, "Error", data['message']);
    }
  }

  void dispose() {
    _identificationController.close();
    _nameController.close();
    _lastNameController.close();
    _phoneController.close();
    _creditsController.close();
    _pictureController.close();
    _loadingData.close();
  }
}
