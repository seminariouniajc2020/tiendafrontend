class Constants {
  static const String urlAuthority = "seminario-segundo-corte.herokuapp.com";
  static const String pathBase = "/api/v1";

  //Api Autentication
  static const String urlAuth = pathBase + "/auth/token";
  static const String urlUserInfo = pathBase + "/auth/userinfo";

  //ContentTypeHeader
  static const String contentTypeJson = "application/json";

  //Api Productos
  static const String consultarProductos = pathBase + "/productos";

//Api Compar
  static const String compras = pathBase + "/compras";

  static const String imageDefault =
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1200px-Circle-icons-profile.svg.png";
}
