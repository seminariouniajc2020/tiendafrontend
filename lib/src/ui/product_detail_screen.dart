import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:tiendafrontend/src/blocs/product_detail_bloc.dart';
import 'package:tiendafrontend/src/models/products.dart';

class ProductDetailScreen extends StatefulWidget {
  final Products producto;
  ProductDetailScreen({Key key, @required this.producto}) : super(key: key);
  @override
  MapScreenState createState() => MapScreenState(producto);
}

class MapScreenState extends State<ProductDetailScreen>
    with SingleTickerProviderStateMixin {
  ProductDetailBloc bloc = ProductDetailBloc();
  Products producto;
  MapScreenState(Products producto) {
    this.producto = producto;
  }

  @override
  void initState() {
    super.initState();
    bloc.changeProduct(producto.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    final coursePrice = Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        "\$" + this.producto.precio.toString(),
        style: TextStyle(color: Colors.white),
      ),
    );

    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 120.0),
        Icon(
          Icons.shopping_cart,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 90.0,
          child: new Divider(color: Colors.green),
        ),
        SizedBox(height: 10.0),
        Text(
          this.producto.nombre,
          style: TextStyle(color: Colors.white, fontSize: 45.0),
        ),
        //SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 6,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      "Talla: " + this.producto.talla,
                      style: TextStyle(color: Colors.white),
                    ))),
            Expanded(flex: 4, child: coursePrice)
          ],
        ),
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(left: 10.0),
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new NetworkImage(this.producto.imagen),
                    fit: BoxFit.cover))),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: topContentText,
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );

    Widget cantidadField(ProductDetailBloc bloc) => StreamBuilder<String>(
          stream: bloc.cantidad,
          builder: (context, snap) {
            return TextField(
              keyboardType: TextInputType.number,
              onChanged: bloc.changeCantidad,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  labelText: "Cantidad",
                  hintText: "Cantidad",
                  prefixIcon: Icon(Icons.shopping_cart),
                  errorText: snap.error),
            );
          },
        );

    final bottomContentText = Text(
      ///lesson.content,
      "Caracteristica: " +
          this.producto.caracteristica.toString() +
          "\n" +
          "Color: " +
          this.producto.color.toString() +
          "\n" +
          "Tipo Producto: " +
          this.producto.tipoProducto.toString(),
      style: TextStyle(fontSize: 18.0),
    );

    Widget submitButton(ProductDetailBloc bloc) => StreamBuilder<bool>(
          stream: bloc.submitValid,
          builder: (context, snap) {
            return Container(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                    onPressed: () => bloc.submit(context),
                    color: Color.fromRGBO(58, 66, 86, 1.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Comprar", style: TextStyle(color: Colors.white)),
                        Icon(
                          Icons.add_circle_outline,
                          color: Colors.white,
                        ),
                      ],
                    )));
          },
        );

    final readButton = Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
            onPressed: () => {},
            color: Color.fromRGBO(58, 66, 86, 1.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Comprar", style: TextStyle(color: Colors.white)),
                Icon(
                  Icons.add_circle_outline,
                  color: Colors.white,
                ),
              ],
            )));

    final bottomContent = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(40.0),
      child: Center(
        child: Column(
          children: <Widget>[
            bottomContentText,
            cantidadField(bloc),
            //readButton
            submitButton(bloc)
          ],
        ),
      ),
    );

    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: <Widget>[topContent, bottomContent],
    )));
  }
}
