import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tiendafrontend/src/Constants.dart';
import 'package:tiendafrontend/src/blocs/profile_bloc.dart';
import 'package:tiendafrontend/src/models/user.dart';

class Profile extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<Profile>
    with SingleTickerProviderStateMixin {
  ProfileBloc bloc = ProfileBloc();
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController _controllerIdentification;
  TextEditingController _controllerName;
  TextEditingController _controllerLastName;
  TextEditingController _controllerPhone;

  @override
  void initState() {
    super.initState();
    bloc.getInfoUser();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Container(
      color: Colors.white,
      child: new ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              new Container(
                height: 250.0,
                color: Colors.white,
                child: new Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: new Stack(fit: StackFit.loose, children: <Widget>[
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[pictureDecoration(bloc)],
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 45.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new CircleAvatar(
                                  backgroundColor: Colors.cyan,
                                  radius: 25.0,
                                  child: new Icon(
                                    Icons.camera_enhance,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            )),
                      ]),
                    )
                  ],
                ),
              ),
              new Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Información Personal',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status ? _getEditIcon() : new Container(),
                                ],
                              )
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Identificación',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 2.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(
                                child: identificationField(bloc),
                              ),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Nombres',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 2.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(child: nameField(bloc)),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Apellidos',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 2.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(
                                child: lastNameField(bloc),
                              ),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  child: new Text(
                                    'Telefono',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                flex: 2,
                              ),
                              Expanded(
                                child: Container(
                                  child: new Text(
                                    'Créditos',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                flex: 2,
                              ),
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 2.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Flexible(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 10.0),
                                  child: phoneField(bloc),
                                ),
                                flex: 2,
                              ),
                              Flexible(
                                child: creditsField(bloc),
                                flex: 2,
                              ),
                            ],
                          )),
                      !_status ? _getActionButtons() : new Container(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    ));
  }

  Widget pictureDecoration(ProfileBloc bloc) => StreamBuilder<User>(
        stream: bloc.userStream,
        builder: (context, snap) {
          return Container(
              width: 140.0,
              height: 140.0,
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                image: new DecorationImage(
                  image: NetworkImage((snap.data == null
                      ? Constants.imageDefault
                      : snap.data.picture)),
                  fit: BoxFit.cover,
                ),
              ));
        },
      );

  Widget identificationField(ProfileBloc bloc) => StreamBuilder<String>(
        stream: bloc.identification,
        builder: (context, snap) {
          _controllerIdentification = new TextEditingController();
          return TextField(
            keyboardType: TextInputType.text,
            //onChanged: bloc.changeIdentification,
            enabled: !_status,
            autofocus: !_status,
            controller: _controllerIdentification
              ..text = (snap.data == null ? "" : snap.data),
            decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                //labelText: "Identificación",
                prefixIcon: Icon(Icons.verified_user),
                errorText: snap.error),
          );
        },
      );

  Widget nameField(ProfileBloc bloc) => StreamBuilder<String>(
        stream: bloc.name,
        builder: (context, snap) {
          _controllerName = new TextEditingController();
          return TextField(
            keyboardType: TextInputType.text,
            //onChanged: bloc.changeName,
            enabled: !_status,
            controller: _controllerName
              ..text = (snap.data == null ? "" : snap.data),
            decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                //labelText: "Nombre",
                prefixIcon: Icon(Icons.text_fields),
                errorText: snap.error),
          );
        },
      );
  Widget lastNameField(ProfileBloc bloc) => StreamBuilder<String>(
        stream: bloc.lastName,
        builder: (context, snap) {
          _controllerLastName = new TextEditingController();
          return TextField(
            keyboardType: TextInputType.text,
            //onChanged: bloc.changeLastName,
            enabled: !_status,
            controller: _controllerLastName
              ..text = (snap.data == null ? "" : snap.data),
            decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                //labelText: "Apellidos",
                prefixIcon: Icon(Icons.text_fields),
                errorText: snap.error),
          );
        },
      );
  Widget phoneField(ProfileBloc bloc) => StreamBuilder<String>(
        stream: bloc.phone,
        builder: (context, snap) {
          _controllerPhone = new TextEditingController();
          return TextField(
            keyboardType: TextInputType.phone,
            //onChanged: bloc.changePhone,
            enabled: !_status,
            controller: _controllerPhone
              ..text = (snap.data == null ? "" : snap.data),
            decoration: InputDecoration(
                //labelText: "Telefono",
                prefixIcon: Icon(Icons.phone),
                errorText: snap.error),
          );
        },
      );

  Widget creditsField(ProfileBloc bloc) => StreamBuilder<String>(
        stream: bloc.credits,
        builder: (context, snap) {
          return TextField(
            keyboardType: TextInputType.number,
            enabled: false,
            onChanged: bloc.changeCredits,
            controller: TextEditingController()
              ..text = (snap.data == null ? "" : snap.data),
            decoration: InputDecoration(
                //labelText: "Creditos",
                prefixIcon: Icon(Icons.monetization_on),
                errorText: snap.error),
          );
        },
      );

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }

  Widget submitButton(ProfileBloc bloc) => StreamBuilder<bool>(
        stream: bloc.submitValid,
        builder: (context, snap) {
          return Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Guardar"),
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  setState(() {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    onchangeDataBloc();
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          );
        },
      );

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          submitButton(bloc),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Cancel"),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.orangeAccent,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }

  onchangeDataBloc() {
    bloc.changeIdentification(_controllerIdentification.text);
    bloc.changeName(_controllerName.text);
    bloc.changeLastName(_controllerLastName.text);
    bloc.changePhone(_controllerPhone.text);
    bloc.submit(context);
  }
}
