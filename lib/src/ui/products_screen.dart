import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tiendafrontend/src/blocs/product_bloc.dart';
import 'package:tiendafrontend/src/models/productsList.dart';
import 'package:tiendafrontend/src/ui/product_detail_screen.dart';

class ProductsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProductsBloc bloc = ProductsBloc();

    bloc.getInfoProducts();

    return Scaffold(
      body: StreamBuilder(
        stream: bloc.infoProducts,
        builder: (context, AsyncSnapshot<ProductsList> snapshot) {
          if (snapshot.hasData) {
            return buildList(snapshot);
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildList(AsyncSnapshot<ProductsList> snapshot) {
    return GridView.builder(
        itemCount: snapshot.data.lstProductos.length,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProductDetailScreen(
                      producto: snapshot.data.lstProductos[index]),
                ),
              );
            },
            child: Card(
              elevation: 4.0,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Image.network(
                      snapshot.data.lstProductos[index].imagen,
                    ),
                    height: 116.0,
                    width: MediaQuery.of(context).size.width / 2.2,
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          snapshot.data.lstProductos[index].nombre,
                          style: TextStyle(fontSize: 16.0, color: Colors.grey),
                        ),
                        SizedBox(
                          height: 2.0,
                        ),
                        Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "\$" + snapshot.data.lstProductos[index].precio.toString(),
                        style: TextStyle(fontSize: 16.0, color: Colors.black),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                     /* Text(
                        "\$" + 50.toString(),
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.grey,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),*/
                      Text(
                        "\$" + 10.toString() + "\% off",
                        style: TextStyle(fontSize: 12.0, color: Colors.grey),
                      ),
                    ],
                  ),
                        SizedBox(
                          height: 8.0,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
