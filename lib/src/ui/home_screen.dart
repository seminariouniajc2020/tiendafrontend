import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tiendafrontend/src/Constants.dart';
import 'package:tiendafrontend/src/blocs/home_bloc.dart';
import 'package:tiendafrontend/src/models/user.dart';
import 'package:tiendafrontend/src/ui/products_screen.dart';
import 'package:tiendafrontend/src/ui/profile_screen.dart';

class DrawerItem {
  String title;
  IconData icon;
  Object body;
  DrawerItem(this.title, this.icon, this.body);
}

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  static HomeBloc bloc = HomeBloc();

  static int _selectDrawerItem = 1;

  static final _drawerItems = [
    DrawerItem("Perfil", Icons.account_circle, Profile()),
    DrawerItem("Productos", Icons.list, ProductsScreen()),
    DrawerItem("Historial de Compras", Icons.history, null),
    DrawerItem("Cerrar Sesión", Icons.exit_to_app, null),
  ];

  _onSelectItem(int itemPos, BuildContext context) {
    setState(() {
      _selectDrawerItem = itemPos;
    });
    Navigator.of(context).pop();
  }

  @override
  initState() {
    super.initState();
    bloc.getInfoUser();
  }

  static _getDrawerItemWidget(int pos) {
    if (pos != _drawerItems.length - 1) {
      return _drawerItems[pos].body;
    } else {
      bloc.logoutUser();
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOptions = [];
    for (var i = 0; i < _drawerItems.length; i++) {
      var d = _drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(d.title),
        selected: i == _selectDrawerItem,
        onTap: () => _onSelectItem(i, context),
      ));
    }

    return drawer(bloc, drawerOptions);
  }

  Widget drawer(HomeBloc bloc, List<Widget> drawerOptions) =>
      StreamBuilder<User>(
          stream: bloc.infoUser,
          builder: (context, snap) {
            return Scaffold(
              appBar: AppBar(
                title: Text("TIENDA"),
              ),
              drawer: Drawer(
                child: Column(children: <Widget>[
                  UserAccountsDrawerHeader(
                    accountName:
                        Text((snap.data == null ? "" : snap.data.name)),
                    accountEmail:
                        Text((snap.data == null ? "" : snap.data.email)),
                    currentAccountPicture: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage((snap.data == null
                                  ? Constants.imageDefault
                                  : snap.data.picture)))),
                    ),
                  ),
                  Column(children: drawerOptions)
                ]),
              ),
              body: _getDrawerItemWidget(_selectDrawerItem),
            );
          });
}
